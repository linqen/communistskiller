﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommunistManager : MonoBehaviour {
	//public Text scoreText;
	int score;
	// Use this for initialization
	void Start () {
		//scoreText.text = ("Score: 0"); 
		score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CommunistClicked(GameObject communist){
		score += 10;
		//scoreText.text = ("Score : " + score);
		Destroy (communist);
	}
}
