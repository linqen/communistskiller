﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommunistGenerator : MonoBehaviour {
	float maxXPos;
	float minXPos;
	float currentTime;
	public GameObject communist;
	public float spawnTime;
	// Use this for initialization
	void Start () {
		currentTime = 0;

		minXPos = transform.position.x - (transform.localScale.x / 2);
		maxXPos = transform.position.x + (transform.localScale.x / 2);
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if (currentTime>=spawnTime) {
			float xSpawnPosition = Random.Range (minXPos, maxXPos);
			Vector3 spawnPoint = new Vector3 (xSpawnPosition, transform.position.y);
			Instantiate (communist, spawnPoint, transform.rotation);
			currentTime = 0;
		}
	}
}
